var gulp = require("gulp"),
    autoprefixer = require("autoprefixer"),
    postcss = require("gulp-postcss"),
    cssvars = require("postcss-simple-vars"),
    nested = require("postcss-nested"),
    cssImport = require("postcss-import"),
    mixins = require("postcss-mixins");

gulp.task('css',function(){
//    console.log("Some Cool Stuff related to sassy CSS or pOst Css  is written here");
    return gulp.src("./app/assets/styles/styles.css").pipe(postcss([cssImport,mixins,cssvars,nested,autoprefixer]))
//        .on('error',function(errorInfo){
//        
//    })
        .pipe(gulp.dest("./app/css"));
    
});
