var gulp = require("gulp");
var watch = require("gulp-watch"),
browserSync = require("browser-sync").create();

gulp.task("watch",function(){
    browserSync.init({
        notify: false,
        server:{
            baseDir:"app"
        }
    });
    watch("./app/index.html", function(){
        browserSync.reload();
    });
    watch("./app/assets/styles/**/*.css",gulp.series('css','cssInject'));
});

gulp.task('cssInject',function(){
    return gulp.src("./app/css/styles.css").pipe(browserSync.stream());
});