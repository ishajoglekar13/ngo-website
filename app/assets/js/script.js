$(document).ready(function(){
  $("#progressElements").owlCarousel({
      center:true,
      items:3,
      autoplay:true,
      margin:20,
      loop:true,
      nav:true,
      smartSpeed:1000,
      autoplayHoverPause:true,
      dots:false,
      navText:['<i class="lni-arrow-left"></i>','<i class="lni-arrow-right"></i>']
  });
});

$(document).ready(function(){
  $("#progressElements1").owlCarousel({
      center:true,
      items:3,
      autoplay:true,
      margin:20,
      loop:true,
      nav:true,
      smartSpeed:1000,
      autoplayHoverPause:true,
      dots:false,
//      navText:['<i class="lni-arrow-left"></i>','<i class="lni-arrow-right"></i>']
  });
});

$(document).ready(function(){
  $("#progressElements2").owlCarousel({
      center:true,
      items:3,
      autoplay:true,
      margin:20,
      loop:true,
      nav:true,
      smartSpeed:1000,
      autoplayHoverPause:true,
      dots:false,
      navText:['<i class="lni-arrow-left"></i>','<i class="lni-arrow-right"></i>']
  });
});




$(document).ready(function(){
  $("#clients").owlCarousel({
      center:true,
      items:6,
      autoplay:true,
      margin:20,
      loop:true,
      nav:true,
      smartSpeed:1000,
      autoplayHoverPause:true,
      dots:false,
      navText:['<i class="lni-arrow-left"></i>','<i class="lni-arrow-right"></i>']
  });
});


$(document).ready(function(){
  $("#cards").owlCarousel({
      center:true,
      items:3,
      autoplay:true,
      margin:20,
      loop:true,
      nav:true,
      smartSpeed:1000,
      autoplayHoverPause:true,
      dots:false,
      navText:['<i class="lni-arrow-left"></i>','<i class="lni-arrow-right"></i>']
  });
});

$(document).ready(function(){
    $("#progressElements").waypoint(function(){
        $(".progress-bar").each(function(){
            
            $(this).animate({
                width:$(this).attr("aria-valuenow")+"%"
            },800);
        });
        this.destroy();
    },{
        offset:'bottom-in-view'
//        offset:'50%'
    });
});

$(document).ready(function(){
    $("#progressElements1").waypoint(function(){
        $(".progress-bar").each(function(){
            
            $(this).animate({
                width:$(this).attr("aria-valuenow")+"%"
            },800);
        });
        this.destroy();
    },{
        offset:'bottom-in-view'
//        offset:'50%'
    });
});

$(document).ready(function(){
    $("#progressElements2").waypoint(function(){
        $(".progress-bar").each(function(){
            
            $(this).animate({
                width:$(this).attr("aria-valuenow")+"%"
            },800);
        });
        this.destroy();
    },{
        offset:'bottom-in-view'
//        offset:'50%'
    });
});


$(document).ready(function(){
    $('#services-tabs').responsiveTabs({
//    startCollapsed: 'accordion'
        animation:'slide'
    });

});